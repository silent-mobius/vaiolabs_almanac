# Pre - Req for Linux and DevOps Course

## General Hardware Pre-Requisite:
	- 4 core CPU with virtualization capablity.
	- 8 Gb of RAM memory.
	- 128 Gb of SSD storage for good performance.
	- Ethernet cable connection or wireless connection
	- Standard internet access 

## General Tools List:
	- Debian Linux ISO: https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.0.0-amd64-netinst.iso
	- Centos Linux ISO: http://centos.fastserv.co.il/7.9.2009/isos/x86_64/CentOS-7-x86_64-DVD-2009.iso
	- Virtualbox : https://www.virtualbox.org/
	- Vagrant : https://www.vagrantup.com/
	- Docker : https://www.docker.com/
	- Kubernetes: https://kubernetes.io/
	
## Additional software:
	- During the class, students will be required to install and configure additional tools.
	- Some software will be installed automatically for students with lab automation.
